#define MAX_KERNEL_RADIUS 20

#define MAX_KERNEL_WIDTH (MAX_KERNEL_RADIUS * 2 + 1)
#define MAX_KERNEL_SIZE (MAX_KERNEL_WIDTH * MAX_KERNEL_WIDTH)

#define MAX_SCALES 5
#define MAX_STEPS 4
#define BASE_SIGMA 1.6
#define PREBLUR_SIGMA 0.5

#define PEAK_THRESHOLD 0.03
#define EDGE_THRESHOLD 5.0

#define BINS 36

#define CURVATURE_THRESHOLD ((EDGE_THRESHOLD + 1.0) * (EDGE_THRESHOLD + 1.0) / EDGE_THRESHOLD)
