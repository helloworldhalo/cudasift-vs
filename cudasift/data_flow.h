#ifndef _DATAFLOW_H_
#define _DATAFLOW_H_

/*!
	\file data_flow.h
	\brief Device <> Host memory allocation and copy wrappers
 */

/*!
	\fn float * allocate_and_copy_device(float * host_data, size_t size)
	\brief Allocates memory and copyes data to device.
	\param host_data Data available on host
	\param size Size of data
	\return Device pointer for copyed data
	\see allocate_and_copy_host()
 */
extern "C" float * allocate_and_copy_device(float * host_data, size_t size);

/*!
	\fn float * allocate_and_copy_host(float * device_data, size_t size)
	\brief Allocates memory and copyes data to host.
	\param device_data Data available on device
	\param size Size of data
	\return Host pointer for copyed data
	\see allocate_and_copy_device()
 */
extern "C" float * allocate_and_copy_host(float * device_data, size_t size);

extern "C" float_image * allocate_and_copy_device_image(float_image * host_data, size_t size);

extern "C" float_image * allocate_and_copy_host_image(float_image * device_data, size_t size);

extern "C" void delete_device(float * device_data); 

#endif