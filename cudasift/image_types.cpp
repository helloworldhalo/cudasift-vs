#include "image_types.h"
#include "data_flow.h"
#include <iostream>

float_image::float_image(unsigned int w, unsigned int h, float_image::Location location) : width(w), height(h), location(location) { }

size_t float_image::size() { return sizeof(float) * width * height; }

cpu_image::cpu_image(unsigned int w, unsigned int h) : float_image(w, h, float_image::Cpu) { }

gpu_image::gpu_image(unsigned int w, unsigned int h) : float_image(w, h, float_image::Gpu) { }

cpu_image::operator gpu_image() {

	gpu_image image(width, height);

	image.data = allocate_and_copy_device(data, size());

	return image;
}

cpu_image::operator gpu_image * () {

	std::cout << "cpu * -> gpu *" << std::endl;

	gpu_image * image = new gpu_image(width, height);

	image->data = allocate_and_copy_device(data, size());

	return image;
}

gpu_image::operator cpu_image() {

	std::cout << "gpu -> cpu" << std::endl;
	
	cpu_image image(width, height);

	image.data = allocate_and_copy_host(data, size());

	return image;
}

gpu_image::operator cpu_image * (){

	cpu_image * image = new cpu_image(width, height);

	image->data = allocate_and_copy_host(data, size());

	return image;
}

gpu_image::operator float * () {

	return allocate_and_copy_host(data, size());
}




