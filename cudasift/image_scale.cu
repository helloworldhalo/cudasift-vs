#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>

#include <math.h>

#include "image_scale.h"
#include "cuda_configuration.h"

__constant__ __device__ int radius, width;

__constant__ __device__ float kernel[MAX_KERNEL_SIZE];

__global__ void resize_kernel(float * in, float * out, int w, int h) {

	int t_x = blockIdx.x * blockDim.x + threadIdx.x;
	int t_y = blockIdx.y * blockDim.y + threadIdx.y;

	float value = in[t_x + t_y * w];

	t_x *= 2;
	t_y *= 2;

	out[t_x + t_y * w * 2] = value;

	out[t_x + 1 + t_y * w * 2] = out[t_x + (t_y + 1) * w * 2] = out[t_x + 1 + (t_y + 1) * w * 2] = 0.f;
}

__global__ void x_interpolation_kernel(float * data, int w, int h) {

	int t_x = blockIdx.x * blockDim.x + threadIdx.x;
	int t_y = blockIdx.y * blockDim.y + threadIdx.y;

	int x, y;

	float value = 0.f;

	for(int i = 0; i < width; i ++) {

		x = t_x * 2 + i - radius + 1;
		
		y = t_y * 2;

		if(x < 0) x = 0;

		if(x >= w - 1) x = w - 1;

		value += kernel[i] * data[x + y * w];
	}

	data[t_x * 2 + 1 + (t_y * 2) * w] = value;
}

__global__ void y_interpolation_kernel(float * data, int w, int h) {

	int t_x = blockIdx.x * blockDim.x + threadIdx.x;
	int t_y = blockIdx.y * blockDim.y + threadIdx.y;

	int x, y;

	float value = 0.f;

	for(int i = 0; i < width; i ++) {

		y = t_y * 2 + i - radius + 1;
		
		x = t_x;

		if(y < 0) y = 0;

		if(y >= h - 1) y = h - 1;

		value += kernel[i] * data[x + y * w];
	}

	data[t_x + (t_y * 2 + 1) * w] = value;
}

__global__ void slice_kernel(float * in, float * out, int w, int h) {

	int t_x = blockIdx.x * blockDim.x + threadIdx.x;
	int t_y = blockIdx.y * blockDim.y + threadIdx.y;

	int x = t_x * 4 + 1;
	int y = t_y * 4 + 1;

	out[t_x + t_y * w / 4] = in[x + y * w];
}

#include <math.h>

float sinc(float x) {

	return sinf(x * 3.14f) / x / 3.14f;
}

resample_kernel_t * get_linear_kernel(int r) {

	float * kernel = new float[2 * r + 1];

	float value;

	for(int i = 0; i < 2 * r + 1; i ++) {

		value = fabs((float)(i - r) / (float)(r + 1));

		if(value < 1.f) kernel[i] = 1 - value;
		
		else kernel[i] = 0;
	}

	return new resample_kernel_t(kernel, r, 2 * r + 1);
}

#include <iostream>

resample_kernel_t * get_lanczos_kernel(int a) {

	int w = 4 * a - 1, r = 2 * a - 1;

	float * line = new float[w];

	float arg, shift = (float)(a) - 0.5f, cutoff = 0.001f;

	for(float x = (float)(-a) + 0.5f; x <= (float)(a) - 0.5f; x += 0.5f) {

		std::cout << "x value : " << x << std::endl;

		std::cout << "position : " << (int)((x + shift) * 2.f) << std::endl;

		if(fabs(floorf(x) - x) <= cutoff) {

			//std::cout << "int values : " << (int)(x + shift) << std::endl;
			
			line[(int)((x + shift) * 2.f)] = 0;

			continue;
		}

		//std::cout << "float values : " << (int)(x + shift) << std::endl;
		
		line[(int)((x + shift) * 2.f)] = sinc(x) * sinc(x / (float)a);

		//else line[x + a * r] = 1.f;

	}
	
	return new resample_kernel_t(line, r, w);	
}

cuda_launch_options get_resize_kernel_configuration(int w, int h) {

	cuda_launch_options option;

	option.threads = dim3(8, 8);
	option.blocks = dim3(w / 16, h / 16);

	return option;
}

cuda_launch_options get_x_interpolation_kernel_configuration(int w, int h) {

	cuda_launch_options option;

	option.threads = dim3(4, 4);
	option.blocks = dim3(w / (2 * option.threads.x), h / (2 * option.threads.y));

	return option;
}

cuda_launch_options get_y_interpolation_kernel_configuration(int w, int h) {

	cuda_launch_options option;

	option.threads = dim3(4, 4);
	option.blocks = dim3(w / (option.threads.x), h / (2 * option.threads.y));

	return option;
}

cuda_launch_options get_slice_kernel_configuration(int w, int h) {

	cuda_launch_options option;

	option.threads = dim3(4, 4);
	option.blocks = dim3(w / (4 * option.threads.x), h / (4 * option.threads.y));

	return option;
}

resample_kernel_t * get_resample_kernel(Resampling type, int p) {

	switch(type) {

		case Resampling::Linear :

			return get_linear_kernel(!p ? 1 : p);

		case Resampling::Lanczos :

			return get_lanczos_kernel(!p ? 2 : p);

		default :
			
			return 0;
	}
}

gpu_image * upsample(gpu_image * image, Resampling type) {

	float * resized_data, * data;

	cudaMalloc((void **)&resized_data, image->size() * 4);

	cudaMalloc((void **)&data, image->size() * 4);

	int w = image->width * 2;
	int h = image->height * 2;

	resample_kernel_t * resample_kernel = get_resample_kernel(type);

	cudaMemcpyToSymbol(kernel, resample_kernel->kernel, sizeof(float) * resample_kernel->width);

	cudaMemcpyToSymbol(radius, &resample_kernel->radius, sizeof(int));

	cudaMemcpyToSymbol(width, &resample_kernel->width, sizeof(int));

	cuda_launch_options op = get_resize_kernel_configuration(w, h);

	resize_kernel<<<op.blocks, op.threads>>>(image->data, resized_data, image->width, image->height);

	op = get_x_interpolation_kernel_configuration(w, h);

	x_interpolation_kernel<<<op.blocks, op.threads>>>(resized_data, w, h);

	op = get_y_interpolation_kernel_configuration(w, h);

	y_interpolation_kernel<<<op.blocks, op.threads>>>(resized_data, w, h);

	gpu_image * scaled_image = new gpu_image(w, h);

	scaled_image->data = resized_data;

	return scaled_image;
}

gpu_image * downsample(gpu_image * image, Resampling type) {

	float * resized_data, * data;

	cudaMalloc((void **)&resized_data, image->size() * 4);

	cudaMalloc((void **)&data, image->size() / 4);

	int w = image->width * 2;
	int h = image->height * 2;

	resample_kernel_t * resample_kernel = get_resample_kernel(type);

	cudaMemcpyToSymbol(kernel, resample_kernel->kernel, sizeof(float) * resample_kernel->width);

	cudaMemcpyToSymbol(radius, &resample_kernel->radius, sizeof(int));

	cudaMemcpyToSymbol(width, &resample_kernel->width, sizeof(int));

	cuda_launch_options op = get_resize_kernel_configuration(w, h);

	resize_kernel<<<op.blocks, op.threads>>>(image->data, resized_data, image->width, image->height);

	op = get_x_interpolation_kernel_configuration(w, h);

	x_interpolation_kernel<<<op.blocks, op.threads>>>(resized_data, w, h);

	op = get_y_interpolation_kernel_configuration(w, h);

	y_interpolation_kernel<<<op.blocks, op.threads>>>(resized_data, w, h);

	op = get_slice_kernel_configuration(w, h);

	slice_kernel<<<op.blocks, op.threads>>>(resized_data, data, w, h);

	gpu_image * scaled_image = new gpu_image(w / 4, h / 4);

	scaled_image->data = data;

	return scaled_image;
}

