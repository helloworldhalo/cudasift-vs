#include <cmath>
#include <malloc.h>
#include <iostream>

#include "sift.h"
#include "gaussian_filter.h"

float get_sigma(int scale) {

	//float sigma = BASE_SIGMA;

	//sigma = sigma * powf(2.0, (float)(scale + step) / (float)MAX_SCALES);

	//return sqrtf(sigma * sigma - PREBLUR_SIGMA * PREBLUR_SIGMA);

	float sigma = sqrtf(2.f) * powf(2.f, (float)(scale - 1) / 2.f);// * sqrtf(powf(2.f, 2.f / (float)(MAX_SCALES - 3)) - 1);

	return sigma;
}

int get_kernel_radius(float sigma) {

	/*int radius = floor(sigma * 3.f + 0.01f);

	return radius > MAX_KERNEL_RADIUS ? MAX_KERNEL_RADIUS : radius;*/

	int i;
    
	for (i = 0; i < MAX_KERNEL_SIZE; i++)

        if(expf(-((float)(i * i)) / (2.f * sigma * sigma)) < 0.001f)
            break;

    //int size = (2 * i - 1) / 2;
    
	return i;
}

int get_kernel_width(int radius) {

	return 2 * radius + 1;
}

int get_kernel_size(int radius) {

	return (2 * radius + 1) * (2 * radius + 1);
}

float get_kernel_value(int x, float sigma) {

	float prepart = 0.159154943;

	float value = prepart / (sigma * sigma) * expf((-1.0) * (float)pow((float)(x - get_kernel_radius(sigma)), 2) / (float)(2 * sigma * sigma));

	return value;
}

float * get_kernel(float sigma) {

	float * k_values, * line_kernel;

	float value, k_sum = 0;

	int width = get_kernel_width(get_kernel_radius(sigma));

	k_values = (float *)malloc(sizeof(float) * width * width);

	line_kernel = (float *)malloc(sizeof(float) * width);

	for(int i = 0; i < width; i ++) line_kernel[i] = get_kernel_value(i, sigma);

	for(int i = 0; i < width; i ++)
		for(int j = 0; j < width; j ++) {

			value = line_kernel[i] * line_kernel[j];

			k_values[i * width + j] = value;

			k_sum += value;
		}

	for(int i = 0; i < width * width; i ++) k_values[i] = k_values[i] / k_sum;

	return k_values;	
}

void print_kernel(float * kernel, int kernel_radius) {

	int kernel_width = (kernel_radius * 2 + 1);

	int kernel_size = (kernel_radius * 2 + 1) * (kernel_radius * 2 + 1);

	for(int i = 0; i < kernel_size; i ++) {
		
		if(i && !(i % kernel_width)) std::cout << std::endl;
		
		printf("%.5f ", kernel[i]);		
	}

	printf("\n");
}