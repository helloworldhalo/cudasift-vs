#ifndef _EXTREMA_DETECTION_H_
#define _EXTREMA_DETECTION_H_

#include "sift.h"
#include "image_types.h"

extern "C" gpu_image * detect_extrema(gpu_image * up, gpu_image * mid, gpu_image * bot);

#endif

