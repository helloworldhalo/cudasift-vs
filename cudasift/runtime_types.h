#ifndef _RUNTIME_TYPES_H_
#define _RUNTIME_TYPES_H_

#include "image_types.h"
#include <vector>

using namespace std;

struct keypoint_t {

	int x, y, scale;

	vector<float> magnitude, orientation;

	keypoint_t(int x, int y, vector<float> magnitude, vector<float> orientation) :
	
		x(x), y(y), orientation(orientation), magnitude(magnitude) {};
	
	~keypoint_t() { magnitude.clear(); orientation.clear(); }
};

struct feature_t {

	int x, y;

	vector<float> feature;
};

struct sift_image_group {

	static struct gpu {
	
		static void foo();
	};

	/* List attributes */

	int index;

	sift_image_group * next;

	/* Host -> Device pointers */

	float_image * origin;

	float * sigmas;

	float_image ** samples;

	float_image ** differences;

	float_image ** extrema;

	float_image ** orientation;

	float_image ** magnitude, ** smoothed_magnitude;

	//vector<keypoint_t> keypoints;

	/* Built-in methods */

	void init();

	void blur();

	void dog();

	void extract_extrema();

	void assign_orientation();

	void extract_keypoints(int idx);

	/* Iteration interface */

	bool iterable();

	sift_image_group * iterate();

	/* Init */

	static sift_image_group * create(float_image * image, int idx = 0);

};

void perform_sift(sift_image_group * target);

void export_all(sift_image_group * target);

#endif