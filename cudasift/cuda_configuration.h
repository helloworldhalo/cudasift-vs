#ifndef _CUDA_CONFIGURATION_H_
#define _CUDA_CONFIGURATION_H_

struct cuda_launch_options {

	dim3 blocks, threads;
};

struct stream_pool {

	int capacity;

	cudaStream_t * pool;
};

static stream_pool gpu_streaming;

#endif